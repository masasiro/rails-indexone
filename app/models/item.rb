class Item < ActiveRecord::Base
  attr_accessible :item_description, :item_name, :tag_list
  acts_as_taggable
end
